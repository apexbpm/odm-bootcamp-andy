package com.apexbpm;


import javax.xml.bind.annotation.XmlType;

@XmlType(name = "Greeting")
public class Greeting
{
	private String target = "";

	public String getTarget()
	{
		return target;
	}

	public void setTarget(String target)
	{
		this.target = target;
	}

}
