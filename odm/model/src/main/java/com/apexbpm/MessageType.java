package com.apexbpm;

import javax.xml.bind.annotation.XmlType;

@XmlType(name="MessageType")
public enum MessageType
{
  None, Information, Warning, Error
}