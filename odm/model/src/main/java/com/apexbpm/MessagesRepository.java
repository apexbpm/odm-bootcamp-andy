package com.apexbpm;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlType;

@XmlType(name = "MessagesRepository")

public class MessagesRepository
{
	private List<Message> messages = new ArrayList<Message>();

	public List<Message> getMessages()
	{
		return messages;
	}

	public void setMessages(List<Message> messages)
	{
		this.messages = messages;
	}
}
