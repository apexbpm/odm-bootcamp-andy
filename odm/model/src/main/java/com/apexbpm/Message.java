package com.apexbpm;

import javax.xml.bind.annotation.XmlType;

@XmlType(name="Message")
public class Message
{
	private String rule = "";
	private String text = "";
	private String reasonCode = "";
	private MessageType type = MessageType.None;

	public String getRule()
	{
		return rule;
	}

	public void setRule(String rule)
	{
		this.rule = rule;
	}

	public String getReasonCode()
	{
		return reasonCode;
	}

	public void setReasonCode(String reasonCode)
	{
		this.reasonCode = reasonCode;
	}

	public MessageType getType()
	{
		return type;
	}

	public void setType(MessageType type)
	{
		this.type = type;
	}

	public String getText()
	{
		return text;
	}

	public void setText(String text)
	{
		this.text = text;
	}
}
