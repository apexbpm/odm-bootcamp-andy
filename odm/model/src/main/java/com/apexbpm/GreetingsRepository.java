package com.apexbpm;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlType;

@XmlType(name = "GreetingsRepository")

public class GreetingsRepository
{
	private List<Greeting> greetings = new ArrayList<Greeting>();

	public List<Greeting> getGreetings()
	{
		return greetings;
	}

	public void setGreetings(List<Greeting> greetings)
	{
		this.greetings = greetings;
	}

}
