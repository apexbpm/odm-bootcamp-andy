package com.apexbpm;

public class Service
{
	private String procedureCode;
	private String baseServiceId;
	private String baseServiceName;
	private int executionOrder;
	private int claimType;
	private int placeOfService;
	private String corporateEntity;
	private String premierBits;
	private String procedureBit;
	private int ageOfMember;
	private String providerType;
	private String diagnosisCode;
	private String diagnosisBits;
	private String serviceCategory;
	private String formatCode;
	private String serviceType;
	private String categoryCode;

	public Service(String procedureCode, int claimType, int placeOfService, String corporateEntity, String premierBits, String procedureBit, int ageOfMember, String providerType, String diagnosisCode, String diagnosisBits) {
		this.procedureCode = procedureCode;
		this.claimType = claimType;
		this.placeOfService = placeOfService;
		this.corporateEntity = corporateEntity;
		this.premierBits = premierBits;
		this.procedureBit = procedureBit;
		this.ageOfMember = ageOfMember;
		this.providerType = providerType;
		this.diagnosisCode = diagnosisCode;
		this.diagnosisBits = diagnosisBits;
	}
	
	public Service() {
	}
	
	public String getProcedureCode()
	{
		return procedureCode;
	}
	
	public void setProcedureCode(String procedureCode)
	{
		this.procedureCode = procedureCode;
	}
	
	public String getBaseServiceId()
	{
		return baseServiceId;
	}
	
	public void setBaseServiceId(String baseServiceId)
	{
		this.baseServiceId = baseServiceId;
	}
	
	public String getBaseServiceName()
	{
		return baseServiceName;
	}
	
	public void setBaseServiceName(String baseServiceName)
	{
		this.baseServiceName = baseServiceName;
	}
	
	public int getClaimType()
	{
		return claimType;
	}

	public void setClaimType(int claimType)
	{
		this.claimType = claimType;
	}

	public int getPlaceOfService()
	{
		return placeOfService;
	}
	
	public void setPlaceOfService(int placeOfService)
	{
		this.placeOfService = placeOfService;
	}
		
	public String getServiceCategory()
	{
		return serviceCategory;
	}

	public void setServiceCategory(String serviceCategory)
	{
		this.serviceCategory = serviceCategory;
	}

	public String getFormatCode()
	{
		return formatCode;
	}

	public void setFormatCode(String formatCode)
	{
		this.formatCode = formatCode;
	}

	public String getServiceType()
	{
		return serviceType;
	}

	public void setServiceType(String serviceType)
	{
		this.serviceType = serviceType;
	}

	public String getCategoryCode()
	{
		return categoryCode;
	}

	public void setCategoryCode(String categoryCode)
	{
		this.categoryCode = categoryCode;
	}

	public int getExecutionOrder()
	{
		return executionOrder;
	}

	public void setExecutionOrder(int executionOrder)
	{
		this.executionOrder = executionOrder;
	}

	public String getCorporateEntity()
	{
		return corporateEntity;
	}

	public void setCorporateEntity(String corporateEntity)
	{
		this.corporateEntity = corporateEntity;
	}

	public String getPremierBits()
	{
		return premierBits;
	}

	public void setPremierBits(String premierBits)
	{
		this.premierBits = premierBits;
	}

	public String getProcedureBit()
	{
		return procedureBit;
	}

	public void setProcedureBit(String procedureBit)
	{
		this.procedureBit = procedureBit;
	}

	public int getAgeOfMember()
	{
		return ageOfMember;
	}

	public void setAgeOfMember(int ageOfMember)
	{
		this.ageOfMember = ageOfMember;
	}

	public String getProviderType()
	{
		return providerType;
	}

	public void setProviderType(String providerType)
	{
		this.providerType = providerType;
	}

	public String getDiagnosisCode()
	{
		return diagnosisCode;
	}

	public void setDiagnosisCode(String diagnosisCode)
	{
		this.diagnosisCode = diagnosisCode;
	}

	public String getDiagnosisBits()
	{
		return diagnosisBits;
	}

	public void setDiagnosisBits(String diagnosisBits)
	{
		this.diagnosisBits = diagnosisBits;
	}
	
}
