package com.apexbpm;

import com.apexbpm.Message;
import com.apexbpm.MessageType;

public class MessageExtender {
	public static void setMessage(Message message, MessageType type,
			String text, String reasonCode, String rule) {
		message.setText(text);
		message.setType(type);
		message.setReasonCode(reasonCode);
		message.setRule(formatRulePath(rule));
	}

	private static String formatRulePath(String str) {
		String s1 = str.replace("_", " ");
		String s2 = s1.replace("$45$", "-");
		return s2.replace(".", "/");
	}
}
