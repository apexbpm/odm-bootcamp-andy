package com.apexbpm;

public class MessagesRepositoryExtender {

	public static void addMessage(MessagesRepository messagesRepository, MessageType type, String message, String reasonCode, String rule) 
	{
		Message msg = new Message();
		MessageExtender.setMessage(msg, type, message, reasonCode, rule);
		messagesRepository.getMessages().add(msg);
	}
}
