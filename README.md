# odm-project-template

This project acts as a standard template for starting ODM projects.  

## best practices
The best practice is to [fork this repository](https://help.github.com/articles/fork-a-repo/) when starting a new  project.  This allows upstream improvements to be folded back into this template.

Next, establish an eclipse workspace directory outside of this repository's directory.  The files in the workspace directory are developer dependent and should not be checked in.

Then you can import the rule projects found in this directory into Rule Designer to start your project.


## project contents

* odm : contains ODM Rule Projects for use within the Rule Designer
    * decision-service : the decision service project
      * resources/eclipse-launch-configurations : these launch configurations can be shared between developers
      * resource/server-list.xml : can be imported into res connections console to establish connection info for deployment targets
    * model : the business object model
    * model-extenders : BOM utilify functions
    * vocabulary : contains the vocabulary elements

* data : used to store data used in the development and testing of the decision service

* docs : project documentation
